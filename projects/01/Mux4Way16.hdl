// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/1/Mux4Way16.hdl
/**
 * 4-way 16-bit multiplexor:
 * Important, array index order from right to left
 * out = a if sel = 00 --> sel[1] = 0, sel[0] = 0
 *       b if sel = 01 --> sel[1] = 0, sel[0] = 1
 *       c if sel = 10 --> sel[1] = 1, sel[0] = 0
 *       d if sel = 11 --> sel[1] = 1, sel[0] = 1
 */
CHIP Mux4Way16 {
    IN a[16], b[16], c[16], d[16], sel[2];
    OUT out[16];
    
    PARTS:
    Mux16(a=a, b=b, sel=sel[0], out=muxab);
    Mux16(a=c, b=d, sel=sel[0], out=muxcd);
    Mux16(a=muxab, b=muxcd, sel=sel[1], out=out);
}

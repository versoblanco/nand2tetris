## About

This repository contains the project assignments from the course [Build a Modern Computer from First Principles: From Nand to Tetris](https://www.coursera.org/learn/build-a-computer/) and the book "The Elements of Computing Systems" by Nisan and Schocken, MIT Press.

I studied a degree in Business Informatics, with approximately a 50% of non technical contents (IT law, finance and accounting, management, etc) so I missed some fundamental hardware-related CS knowledge. As I am now following a path in Software Development, I hope to cover some gaps with self-learning material like this course and other resources.

The Software Suite for the course is not included in this repo. Tools are available for download under https://www.nand2tetris.org/software or you can just use the more recent and recommended web IDE https://nand2tetris.github.io/web-ide/chip/ (Source Code: https://github.com/nand2tetris/web-ide)

## Would you like to know more?

There are some games inspired in this course, like https://nandgame.com/

Whenever this course/book is mentioned, there is always someone who also recommends [Code: the hidden Language of Computer Hardware and Software](https://search.worldcat.org/search?q=ti%3ACode+AND+au%3ACharles+Petzold&inLanguage=eng&itemType=book) from Charles Petzold. I find better to read the first Chapters before doing the course.

A series of well made, fast-paced videos with a similar bottom-up approach to general understanding of how computers works is [Crash Course Computer Science](https://piped.kavin.rocks/playlist?list=PL8dPuuaLjXtNlUrzyH5r6jN9ulIgZBpdo)

For the lower level, the [Digital Electronics Course](https://piped.kavin.rocks/playlist?list=PLX7ZPgPJWZNc6zRvIGjDFOr6CYKz9DZa3) from the Columbia Gorge Community College has excellent explanations, very helpful with [Boolean Algebra](https://piped.kavin.rocks/watch?v=hQefZ6d9m2k&list=PLX7ZPgPJWZNc6zRvIGjDFOr6CYKz9DZa3&index=26).

I also looked up https://electronics-course.com/ for some references, but I have not really dive into it.

As the computer in the course is only emulated with assistance of a Software tool, some people go afterwards for Ben Eater's [Build an 8-bit computer from scratch](https://eater.net/8bit/) to actually build a physical one on breadboards.
